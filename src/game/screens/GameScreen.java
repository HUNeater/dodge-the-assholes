package game.screens;

import com.jumi.JUMILoader;
import com.jumi.scene.JUMIScene;
import com.jumi.scene.objects.JUMIMesh;
import game.*;
import game.objects.*;
import game.objects.Object;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class GameScreen extends Screen {
    private GameLogic gameLogic;
    private static List<Object> objects;
    private List<Object> lights;
    private Skybox skybox;
    public static Shader objectShader, skyboxShader;
    private static Mesh player;

    public GameScreen(Game game) {
        super(game);

        objects = new ArrayList<>();
        lights = new ArrayList<>();

        objectShader = new Shader("Object shader",
                Utils.loadResource("vertexShader.glsl"),
                Utils.loadResource("fragmentShader.glsl"));

        skyboxShader = new Shader("Skybox shader",
                Utils.loadResource("skybox/vertex.glsl"),
                Utils.loadResource("skybox/fragment.glsl"));

        PointLight light = new PointLight(new Vector3f(0f, 2f, -5f));
        light.setColor(new Vector3f(1f, 1f, 1f));

        lights.add(light);

        skybox = new Skybox(new String[]{
                "skybox/starfield_rt.tga",
                "skybox/starfield_lf.tga",
                "skybox/starfield_up.tga",
                "skybox/starfield_dn.tga",
                "skybox/starfield_bk.tga",
                "skybox/starfield_ft.tga",
        }, game);

        JUMIScene scene = JUMILoader.loadModel("ship.obj");
        JUMIMesh playerMesh = null;
        if (scene != null) {
            playerMesh = scene.getMeshByIndex(0);
        }

        if (playerMesh != null) {
            for (JUMIMesh a : scene.getAllMeshes()) {
                a.triangulate();
                a.flipUVY();
            }

            player = new Mesh(
                    playerMesh.getVertices(),
                    playerMesh.getVertexIndices(),
                    playerMesh.getNormals());

            player.setColor(new Vector3f(0.886f, 0.588f, 0.294f));
            player.setShowCollision(true);
        }
    }

    @Override
    public void show() {
        gameLogic = new GameLogic(game);
    }

    @Override
    public void render() {
        skyboxShader.use();
        skybox.render();

        objectShader.use();
        objects.forEach(Object::render);
        lights.forEach(Object::render);
        player.render();
    }

    @Override
    public void tick(double delta) {
        super.tick(delta);
        gameLogic.tick(delta);
    }

    @Override
    public void resize(int width, int height) {
        getCamera().resize(width, height);
    }

    @Override
    public void cleanup() {
        objects.forEach(Object::cleanup);
        objects.clear();

        lights.forEach(Object::cleanup);
        lights.clear();

//        player.cleanup();
//        skybox.cleanup();
        gameLogic.cleanup();
    }

    public static List<Object> getObjects() {
        return objects;
    }

    public static Mesh getPlayer() {
        return player;
    }
}