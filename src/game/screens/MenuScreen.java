package game.screens;

import game.Game;
import game.Texture;
import game.objects.Object;
import game.objects.Pane;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;

public class MenuScreen extends Screen {
    private Pane pane;

    public MenuScreen(Game game) {
        super(game);

        Texture t = new Texture("menu.png");

        pane = new Pane(26, -80, 8, new Texture("menu.png"));
        pane.addRotation(10, 25, -90f);
        pane.setShadingType(Object.ShadingTypes.FLAT);
        pane.setSize(40, 40, 1);

        t.cleanup();
    }

    @Override
    public void show() {

    }

    @Override
    public void render() {
        GameScreen.objectShader.use();
        pane.render();
    }

    @Override
    public void tick(double delta) {
        super.tick(delta);

        if (game.input.isKeyPressed(GLFW_KEY_ENTER))
            game.setActiveScreen(Game.GameStates.GAME);
    }

    @Override
    public void resize(int width, int height) {
        getCamera().resize(width, height);
    }

    @Override
    public void cleanup() {
        pane.cleanup();
    }
}