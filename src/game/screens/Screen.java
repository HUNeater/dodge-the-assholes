package game.screens;

import game.Game;
import game.objects.Camera;

public abstract class Screen {
    public Game game;
    Camera cam;

    public abstract void show();

    public abstract void render();

    public abstract void resize(int width, int height);

    public abstract void cleanup();

    public Screen(Game game) {
        this.game = game;
    }

    public void tick(double delta) {
        game.getCamera().update();
    }

    public Camera getCamera() {
        return game.getCamera();
    }
}