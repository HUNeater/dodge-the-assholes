package game;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Shader {
    private int programId, vxId, frId;
    private boolean using = false;
    private String name;

    public Shader(String name, String vxCode, String frCode) {
        this.name = name;

        programId = glCreateProgram();
        if (programId == NULL)
            throw new RuntimeException("Failed to create shader program");

        vxId = createShader(vxCode, GL_VERTEX_SHADER);
        frId = createShader(frCode, GL_FRAGMENT_SHADER);

        glLinkProgram(programId);
        if (glGetProgrami(programId, GL_LINK_STATUS) == NULL)
            throw new RuntimeException("Failed to link shader program");

        glValidateProgram(programId);
        if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0)
            throw new RuntimeException(glGetShaderInfoLog(programId, 1024));
    }

    private int createShader(String shaderCode, int shaderType) {
        int shaderId = glCreateShader(shaderType);
        if (shaderId == NULL)
            throw new RuntimeException(glGetShaderInfoLog(programId, 1024));

        glShaderSource(shaderId, shaderCode);
        glCompileShader(shaderId);
        if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == NULL)
            throw new RuntimeException(glGetShaderInfoLog(shaderId, 1024));

        glAttachShader(programId, shaderId);

        return shaderId;
    }

    public void cleanup() {
        unuse();

        if (programId != NULL) {
            if (vxId != NULL)
                glDetachShader(programId, vxId);

            if (frId != NULL)
                glDetachShader(programId, frId);

            glDeleteProgram(programId);
        }
    }

    public void use() {
        if (!using) {
            glUseProgram(programId);
            using = true;
        }
    }

    public void unuse() {
        if (using) {
            glUseProgram(0);
            using = false;
        }
    }

    public int getId() {
        return programId;
    }

    public boolean isInUse() {
        return using;
    }
}