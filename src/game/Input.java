package game;

import static org.lwjgl.glfw.GLFW.*;

public class Input {
    private boolean initMouse = true;
    private boolean[] keys = new boolean[1024];
    private double mouseX, mouseY;
    public double x, y;
    public long w;

    public Input(long w) {
        this.w = w;

        // Mouse callback
        glfwSetCursorPosCallback(w, (window, x, y) -> {
            if (initMouse) {
                mouseX = x;
                mouseY = y;

                initMouse = false;
            }

            this.x = x;
            this.y = y;
        });

        // Key callback
        glfwSetKeyCallback(w, (window, key, scancode, action, mods) -> {
            if (action == GLFW_PRESS)
                keys[key] = true;
            else if (action == GLFW_RELEASE)
                keys[key] = false;
        });
    }

    public void update(double delta) {
//        double mouseSensitivity = 0.05f;
//        double mouseDx = (x - mouseX) * mouseSensitivity;
//        double mouseDy = (mouseY - y) * mouseSensitivity;
//
//        mouseX = x;
//        mouseY = y;

//        cam.update(mouseDx, mouseDy);

        if (keys[GLFW_KEY_ESCAPE])
            glfwSetWindowShouldClose(w, true);

//        if (keys[GLFW_KEY_W])
//            cam.moveForward((float) (1f * delta));
//        if (keys[GLFW_KEY_S])
//            cam.moveForward((float) (-1f * delta));
    }

    public boolean isKeyPressed(int key) {
        return keys[key];
    }
}