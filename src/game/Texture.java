package game;

import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.stb.STBImage.*;

public class Texture {
    private IntBuffer width = BufferUtils.createIntBuffer(1);
    private IntBuffer height = BufferUtils.createIntBuffer(1);
    private IntBuffer comp = BufferUtils.createIntBuffer(1);
    private ByteBuffer img;

    public Texture(String file) {
        ByteBuffer imgBuffer = null;

        try {
            imgBuffer = Utils.ioResourceToByteBuffer(file, 8 * 1024);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (imgBuffer != null)
            img = stbi_load_from_memory(imgBuffer, width, height, comp, 0);
        else
            throw new RuntimeException(stbi_failure_reason());
    }

    public void cleanup() {
        stbi_image_free(img);
    }

    public int getWidth() {
        return width.get(0);
    }

    public int getHeight() {
        return height.get(0);
    }

    public int getComp() {
        return comp.get(0);
    }

    public ByteBuffer getImg() {
        return img;
    }
}
