package game.objects;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class Mesh extends Object {
    private int length;

    public Mesh(float[] verts, int[] indicies, float[] normals) {
        length = indicies.length;

        FloatBuffer vertsBuffer = BufferUtils.createFloatBuffer(verts.length + normals.length);
        int normal = 0;
        int vertex = 0;
        int pos = 0;
        for (int i = 1; i < normals.length / 3; i += 1) {
            vertsBuffer.put(pos, verts[vertex]);
            vertsBuffer.put(pos + 1, verts[vertex + 1]);
            vertsBuffer.put(pos + 2, verts[vertex + 2]);
            vertex += 3;
            pos += 3;

            vertsBuffer.put(pos, normals[normal]);
            vertsBuffer.put(pos + 1, normals[normal + 1]);
            vertsBuffer.put(pos + 2, normals[normal + 2]);
            normal += 3;
            pos += 3;
        }

        IntBuffer indsBuffer = BufferUtils.createIntBuffer(indicies.length);
        indsBuffer.put(indicies).flip();

        VAO = glGenVertexArrays();
        VBO = glGenBuffers();
        EBO = glGenBuffers();

        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertsBuffer, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indsBuffer, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, false, 4 * 6, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, false, 4 * 6, 12);
        glEnableVertexAttribArray(1);

        glBindVertexArray(0);

        collidable = new Box();
        collidable.setRenderMode(RenderModes.LINE);
        collidable.setShadingType(ShadingTypes.FLAT);
    }

    @Override
    public void draw() {
        glDrawElements(GL_TRIANGLES, length, GL_UNSIGNED_INT, 0);
    }
}