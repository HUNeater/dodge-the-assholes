package game.objects;

import game.screens.GameScreen;
import org.joml.AxisAngle4f;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.system.MemoryUtil.NULL;

public abstract class Object extends Base {
    private Vector3f size;
    private Vector3f relativePos;
    private Vector3f ambient;
    private RenderModes renderMode;
    private ShadingTypes shading;
    private boolean showCollision;
    boolean isVisible;
    int VAO, VBO, EBO, TBO;
    Vector3f color;
    Vector3f worldPos;
    Object collidable;
    Quaternionf rotation;
    Matrix4f modelMatrix;

    public enum ShadingTypes {
        SHADED,
        FLAT,
    }

    enum RenderModes {
        FILL,
        LINE,
    }

    // Defaults
    public Object() {
        color = new Vector3f();
        ambient = new Vector3f();
        size = new Vector3f(1, 1, 1);
        worldPos = new Vector3f();
        relativePos = new Vector3f();
        modelMatrix = new Matrix4f();
        rotation = new Quaternionf();
        renderMode = RenderModes.FILL;
        shading = ShadingTypes.SHADED;
        showCollision = false;
        isVisible = true;

        setColor(1, 1, 1);
    }

    public abstract void draw();

    public void render() {
        tick();

        glBindVertexArray(VAO);

        if (renderMode == RenderModes.FILL)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        if (TBO != NULL) glBindTexture(GL_TEXTURE_2D, TBO);
        if (isVisible) draw();
        if (TBO != NULL) glBindTexture(GL_TEXTURE_2D, TBO);
        glBindVertexArray(0);
    }

    public void tick() {
        if (collidable != null) {
            collidable.setLocation(worldPos);

            if (showCollision)
                collidable.render();
        }

        if (TBO != NULL)
            uniform(GameScreen.objectShader, "useTexture", GL_TRUE);
        else
            uniform(GameScreen.objectShader, "useTexture", GL_FALSE);

        modelMatrix.identity();
        modelMatrix.translate(worldPos);
        modelMatrix.translate(relativePos);
        modelMatrix.translate(0.5f * size.x, 0.5f * size.y, 0f);
        modelMatrix.rotate(rotation);
        modelMatrix.translate(-0.5f * size.x, -0.5f * size.y, 0f);
        modelMatrix.scale(size);

        uniform(GameScreen.objectShader, "model", modelMatrix);
        uniform(GameScreen.objectShader, "mat.ambient", ambient);
        uniform(GameScreen.objectShader, "mat.color", color);
        uniform(GameScreen.objectShader, "mat.specular", 0.5f, 0.5f, 0.5f);
        uniform(GameScreen.objectShader, "mat.shininess", 32f);

        if (shading == ShadingTypes.FLAT)
            uniform(GameScreen.objectShader, "useFlatShading", GL_TRUE);
        else
            uniform(GameScreen.objectShader, "useFlatShading", GL_FALSE);
    }

    public void cleanup() {
        if (VAO != NULL)
            glDeleteVertexArrays(VAO);
        if (VBO != NULL)
            glDeleteBuffers(VBO);
        if (EBO != NULL)
            glDeleteBuffers(EBO);
        if (TBO != NULL)
            glDeleteBuffers(TBO);
    }

    public boolean intersects(Object object) {
        if (collidable != null && object.getCollidable() != null) {
            Vector3f ol = object.getCollidable().getLocation();
            Vector3f os = object.getCollidable().getSize();

            if (Math.abs(collidable.worldPos.x - ol.x) < collidable.size.x + os.x) {
                if (Math.abs(collidable.worldPos.y - ol.y) < collidable.size.y + os.y) {
                    if (Math.abs(collidable.worldPos.z - ol.z) < collidable.size.z + os.z) {
                        collidable.setColor(1, 0, 0);

                        return true;
                    }
                }
            }

            collidable.setColor(new Vector3f(1, 1, 1));
            return false;
        }

        return false;
    }

    public void addRotation(float x, float y, float z) {
        Quaternionf quaternion = new Quaternionf();
        quaternion.rotate(
                (float) Math.toRadians(x),
                (float) Math.toRadians(y),
                (float) Math.toRadians(z)
        );

        rotation.add(quaternion);
    }

    public void addWorldLocation(float x, float y, float z) {
        worldPos.x += x;
        worldPos.y += y;
        worldPos.z += z;
    }

    public void setLocation(Vector3f loc) {
        this.worldPos = loc;
    }

    public void setRelativeLocation(float x, float y, float z) {
        relativePos.x = x;
        relativePos.y = y;
        relativePos.z = z;
    }

    public void setRenderMode(RenderModes mode) {
        renderMode = mode;
    }

    public void setShowCollision(boolean b) {
        showCollision = b;
    }

    public void setColor(Vector3f color) {
        this.color = color;

        ambient = color;
        ambient.mul(0.5f);
    }

    public void setColor(float r, float g, float b) {
        color.x = r;
        color.y = g;
        color.z = b;

        ambient = color;
        ambient.mul(0.5f);
    }

    public void setRotation(AxisAngle4f rotation) {
//        this.rotation = rotation;
        this.rotation.set(rotation);
    }

    public void setSize(Vector3f size) {
        this.size = size;
    }

    public void setSize(float x, float y, float z) {
        size.x = x;
        size.y = y;
        size.z = z;
    }

    public void setShadingType(ShadingTypes type) {
        shading = type;
    }

    public void setVisible(boolean b) {
        isVisible = b;
    }

    public Object getCollidable() {
        return collidable;
    }

    public Vector3f getLocation() {
        return worldPos;
    }

    public Vector3f getSize() {
        return size;
    }
}