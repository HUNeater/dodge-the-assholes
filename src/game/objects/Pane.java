package game.objects;

import game.Texture;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.*;

public class Pane extends Object {

    public Pane(Vector3f pos, ByteBuffer img, int comp, int width, int height) {
        this.worldPos = pos;

        float[] vertices = new float[]{
                0.5f, 0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
                -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
                -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
                0.5f, -0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f
        };
        FloatBuffer vertBuff = BufferUtils.createFloatBuffer(vertices.length);
        vertBuff.put(vertices).flip();

        int[] indices = new int[]{0, 1, 2, 0, 2, 3};
        IntBuffer indBuff = BufferUtils.createIntBuffer(indices.length);
        indBuff.put(indices).flip();

        VAO = glGenVertexArrays();
        VBO = glGenBuffers();
        EBO = glGenBuffers();
        TBO = glGenTextures();

        glBindVertexArray(VAO);

        glBindTexture(GL_TEXTURE_2D, TBO);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (comp == 3)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
        else if (comp == 4)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);

        glGenerateMipmap(GL_TEXTURE_2D);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertBuff, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indBuff, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, false, 8 * 4, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, false, 8 * 4, 12);
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 2, GL_FLOAT, false, 8 * 4, 24);
        glEnableVertexAttribArray(2);

        glBindVertexArray(0);

        setupCollidable();
    }

    public Pane(float x, float y, float z) {
        this(new Vector3f(x, y, z));
    }

    public Pane(float x, float y, float z, Texture texture) {
        this(new Vector3f(x, y, z), texture.getImg(), texture.getComp(), texture.getWidth(), texture.getHeight());
    }

    public Pane(Vector3f pos) {
        this.worldPos = pos;

        float[] vertices = new float[]{
                0.5f, 0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
                -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
                -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
                0.5f, -0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f
        };
        FloatBuffer vertBuff = BufferUtils.createFloatBuffer(vertices.length);
        vertBuff.put(vertices).flip();

        int[] indices = new int[]{0, 1, 2, 0, 2, 3};
        IntBuffer indBuff = BufferUtils.createIntBuffer(indices.length);
        indBuff.put(indices).flip();

        VAO = glGenVertexArrays();
        VBO = glGenBuffers();
        EBO = glGenBuffers();

        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertBuff, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indBuff, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, false, 8 * 4, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, false, 8 * 4, 12);
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 2, GL_FLOAT, false, 8 * 4, 24);
        glEnableVertexAttribArray(2);

        glBindVertexArray(0);

        setupCollidable();
    }

    private void setupCollidable() {
        collidable = new Box();
        collidable.setRenderMode(RenderModes.LINE);
        collidable.setShadingType(ShadingTypes.FLAT);
        collidable.setSize(0.5f, 0.5f, 0.1f);
        collidable.setRelativeLocation(0, 1, 0);
    }

    @Override
    public void draw() {
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
}