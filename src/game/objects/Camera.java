package game.objects;

import game.Shader;
import game.screens.GameScreen;
import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

public class Camera extends Base {
    private Matrix4f viewMatrix;
    private Vector3f pos = new Vector3f(0f, 0f, 0f);
    private Vector3f up = new Vector3f(0f, 1f, 0f);
    private Vector3f dir = new Vector3f(0f, 0f, 0f);

    private float fov = 45f;
    private float camSpeed = 10f;
    private double pitch, yaw = 90f;
    private int width, height;

    public Camera(Vector3f pos) {
        this.pos = pos;
    }

    public void update() {
//        yaw += dx;
//        pitch += dy;
//
//        if (pitch >= 89)
//            pitch = 89;
//        if (pitch <= -89)
//            pitch = -89;
//
//        dir.x = (float) (Math.cos(Math.toRadians(pitch)) * Math.cos(Math.toRadians(yaw)));
//        dir.y = (float) Math.sin(Math.toRadians(pitch));
//        dir.z = (float) (Math.cos(Math.toRadians(pitch)) * Math.sin(Math.toRadians(yaw)));
//        dir.normalize().add(worldPos);

        Vector3f target = GameScreen.getPlayer().getLocation();
        pos = new Vector3f(target.x, target.y + 4, target.z - 10);

        float rad = 1;
        float speed = 0.5f;
        pos.x += (float) (Math.sin(glfwGetTime() * speed) * rad);
        pos.y += (float) (Math.sin(glfwGetTime() * speed) * rad);
        pos.z += (float) Math.cos(glfwGetTime() * speed) * rad;

//        float maxFov = 90f;
//        float minFov = 45f;
//        float fovSpeed = 1f;
//        float mul = (maxFov - minFov) / 2;
//        float trans = minFov + mul;
//        setFov((float) (Math.toRadians(Math.sin(glfwGetTime() * fovSpeed) * mul + trans)));

        // view matrices
        viewMatrix = new Matrix4f().lookAt(pos, target, up);

        uniform(GameScreen.objectShader, "view", viewMatrix);
        uniform(GameScreen.objectShader, "viewPos", pos);

        Matrix4f v = new Matrix4f(new Matrix3f(viewMatrix));
        uniform(GameScreen.skyboxShader, "view", v);
    }

    public void resize(int width, int height) {
        this.width = width;
        this.height = height;

        uniform(GameScreen.objectShader, "projection",
                new Matrix4f().perspective(fov, (float) width / height, 0.1f, 100f));

        uniform(GameScreen.skyboxShader, "projection",
                new Matrix4f().perspective(fov, (float) width / height, 0.1f, 100f));
    }

    public void moveForward(float d) {
        pos.add(getForwardVector().mul(camSpeed * d));
    }

    public void moveRight(float d) {
        pos.add(getRightVector().mul(camSpeed * d));
    }

    private Vector3f getForwardVector() {
        Vector3f fwd = dir.sub(pos);
        fwd.normalize();

        return fwd;
    }

    private Vector3f getRightVector() {
        Vector3f right = getForwardVector().cross(up);
        right.normalize();

        return right;
    }

    @Override
    void uniform(Shader shader, String var, Vector3f val) {
        super.uniform(shader, var, val);
        shader.unuse();
    }

    @Override
    void uniform(Shader shader, String var, Matrix4f val) {
        super.uniform(shader, var, val);
        shader.unuse();
    }

    Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    private void setFov(float fov) {
        this.fov = fov;
        resize(width, height);
    }
}