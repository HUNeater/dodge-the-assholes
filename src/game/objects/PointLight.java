package game.objects;

import game.screens.GameScreen;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class PointLight extends Object {
    public PointLight(Vector3f pos) {
        this.worldPos = pos;

        float[] vertices = new float[]{
                0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
                -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
                -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
                0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f
        };
        FloatBuffer vertBuff = BufferUtils.createFloatBuffer(vertices.length);
        vertBuff.put(vertices).flip();

        int[] indices = new int[]{
                0, 1, 2,
                0, 2, 3
        };
        IntBuffer indBuff = BufferUtils.createIntBuffer(indices.length);
        indBuff.put(indices).flip();

        VAO = glGenVertexArrays();
        VBO = glGenBuffers();
        EBO = glGenBuffers();

        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertBuff, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indBuff, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, false, 6 * 4, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, false, 6 * 4, 12);
        glEnableVertexAttribArray(1);

        glBindVertexArray(0);

        isVisible = false;
    }

    @Override
    public void draw() {
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }

    @Override
    public void tick() {
        super.tick();

//        float rad = 2;
//        float speed = 5;
//
//        worldPos.x = (float) Math.sin(glfwGetTime() * speed) * rad;
//        worldPos.y = (float) Math.sin(glfwGetTime() * speed) * rad;
//        worldPos.z = (float) Math.cos(glfwGetTime() * speed) * rad;
//
//        Matrix4f matrix = new Matrix4f();
//        matrix.translate(worldPos);
//        matrix.rotate(rotation);
//        matrix.scale(size);
//
//        uniform(Game.objectShader, "model", matrix);
//        uniform(Game.objectShader, "mat.ambient", color);
//        uniform(Game.objectShader, "mat.color", color);
//        uniform(Game.objectShader, "mat.specular", 0.5f, 0.5f, 0.5f);
//        uniform(Game.objectShader, "mat.shininess", 32f);

        uniform(GameScreen.objectShader, "light.pos", worldPos);
        uniform(GameScreen.objectShader, "light.ambient", color);
        uniform(GameScreen.objectShader, "light.diffuse", color);
        uniform(GameScreen.objectShader, "light.specular", 0.5f, 0.5f, 0.5f);
        uniform(GameScreen.objectShader, "light.constant", 1f);
        uniform(GameScreen.objectShader, "light.linear", 0.022f);
        uniform(GameScreen.objectShader, "light.quadratic", 0.0019f);
    }
}