package game.objects;

import game.Shader;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public abstract class Base {
    void uniform(Shader shader, String var, Vector3f val) {
        int loc = glGetUniformLocation(shader.getId(), var);
        if (loc != -1) {
            shader.use();
            glUniform3f(loc, val.x, val.y, val.z);
        } else {
            printError(var);
        }
    }

    void uniform(Shader shader, String var, float x, float y, float z) {
        int loc = glGetUniformLocation(shader.getId(), var);
        if (loc != -1) {
            shader.use();
            glUniform3f(loc, x, y, z);
        } else {
            printError(var);
        }
    }

    void uniform(Shader shader, String var, int val) {
        int loc = glGetUniformLocation(shader.getId(), var);
        if (loc != -1) {
            shader.use();
            glUniform1i(loc, val);
        } else {
            printError(var);
        }
    }

    void uniform(Shader shader, String var, float x) {
        int loc = glGetUniformLocation(shader.getId(), var);
        if (loc != -1) {
            shader.use();
            glUniform1f(loc, x);
        } else {
            printError(var);
        }
    }

    void uniform(Shader shader, String var, Matrix4f val) {
        int loc = glGetUniformLocation(shader.getId(), var);
        if (loc != -1) {
            FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
            val.get(buffer);

            shader.use();
            glUniformMatrix4fv(loc, false, buffer);
        } else {
            printError(var);
        }
    }

    private void printError(String var) {
        System.out.println("uniform '" + var + "' not found");
    }
}