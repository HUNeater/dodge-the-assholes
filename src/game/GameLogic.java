package game;

import game.objects.Object;
import game.objects.Pane;
import game.screens.GameScreen;
import org.joml.AxisAngle4f;
import org.joml.Vector3f;

import java.util.Iterator;
import java.util.Random;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class GameLogic {
    private Game game;
    private Random rand = new Random();
    private boolean waveInProgress = false;
    private int spawnedEnemies = 0;
    private int wave = -1;
    private Texture t;

    // amount, timer
    private float[][] waves = {
            {5, 1.0f},
            {10, 1.0f},
            {20, 0.8f},
            {25, 0.7f},
            {50, 0.6f}, // 5
            {60, 0.5f},
            {70, 0.4f},
            {80, 0.3f},
            {90, 0.2f},
            {120, 0.1f}, // 10
    };
    private double oldTime = glfwGetTime();
    private double timer = NULL;
    private double spawnTimer = NULL;

    public GameLogic(Game game) {
        this.game = game;
        t = new Texture("tex.png");
    }

    public void tick(double delta) {
        // timing
        double timeNow = glfwGetTime();
        double elapsedTime = timeNow - oldTime;
        oldTime = timeNow;

        if (!waveInProgress) timer += elapsedTime;
        if (waveInProgress) spawnTimer += elapsedTime;

        handleMovement(delta);

        if (waveInProgress) {
            // moving objects
            for (Object object : GameScreen.getObjects()) {
                object.addWorldLocation(0, 0, (float) (-10 * delta));
            }

            // removing objects
            for (Iterator<Object> iter = GameScreen.getObjects().iterator(); iter.hasNext(); ) {
                Object object = iter.next();

                if (object.getLocation().z < -20f) {
                    iter.remove();
                }

                // Collision detection
                if (object.intersects(GameScreen.getPlayer())) {
                    game.setActiveScreen(Game.GameStates.DEATH);
                    break;
                }
            }

            // spawning objects
            if (spawnTimer > waves[wave][1]) {
                spawnEnemies();
            }

            // end of wave
            if (spawnedEnemies >= waves[wave][0] && GameScreen.getObjects().size() == 0) {
                waveInProgress = false;
                spawnedEnemies = 0;
            }
        } else {
            // other waves
            if (timer > 5 && (wave + 1) < waves.length) {
                wave++;
                waveInProgress = true;
                timer = NULL;
                System.out.println("wave: " + (wave + 1));
            }
        }
    }

    private void handleMovement(double delta) {
//        double mouseSensitivity = 0.05f;
//        double mouseDx = (x - mouseX) * mouseSensitivity;
//        double mouseDy = (mouseY - y) * mouseSensitivity;
//
//        mouseX = x;
//        mouseY = y;

//        if (keys[GLFW_KEY_W])
//            cam.moveForward((float) (1f * delta));
//        if (keys[GLFW_KEY_S])
//            cam.moveForward((float) (-1f * delta));

        float x = GameScreen.getPlayer().getLocation().x;
        float m = (float) (10f * delta);

        // move right
        if (game.input.isKeyPressed(GLFW_KEY_D) || game.input.isKeyPressed(GLFW_KEY_RIGHT)) {
            if (x - m > -10)
                GameScreen.getPlayer().addWorldLocation(-m, 0f, 0f);
        }

        // move left
        if (game.input.isKeyPressed(GLFW_KEY_A) || game.input.isKeyPressed(GLFW_KEY_LEFT)) {
            if (x + m < 10)
                GameScreen.getPlayer().addWorldLocation(m, 0f, 0f);
        }
    }

    private void spawnEnemies() {
        if (spawnedEnemies < waves[wave][0]) {
            Pane pane = new Pane(rand.nextFloat() * 20 - 10, -0.5f, 20f, t);
            pane.setRotation(new AxisAngle4f((float) Math.toRadians(-90f), 0, 0, 1));
            pane.setSize(new Vector3f(1, 1, 1));
            pane.setShowCollision(false);

            GameScreen.getObjects().add(pane);
            spawnedEnemies++;
        }

        spawnTimer = NULL;
    }

    public void cleanup() {
        t.cleanup();
    }
}