package game;

import game.objects.Camera;
import game.screens.DeathScreen;
import game.screens.GameScreen;
import game.screens.MenuScreen;
import game.screens.Screen;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

@SuppressWarnings("ConstantConditions")
public class Game {
    private static final String TITLE = "Dodge the assholes!";
    private static final boolean VSYNC = false;
    private static final boolean MSAA = true;
    private static final int SAMPLES = 4;
    private int width = 1280, height = 720;
    private boolean resized = false;
    private long window;
    private Screen menuScreen, gameScreen, deathScreen;
    private GameStates gameState = GameStates.MENU;
    public Input input;
    private Camera cam = new Camera(new Vector3f(0f, 10f, -10f));
    public Sounds sounds;

    public enum GameStates {
        MENU,
        GAME,
        DEATH
    }

    private void tick(double delta) {
        input.update(delta);
        getActiveScreen().tick(delta);
    }

    private void render() {
        getActiveScreen().render();
    }

    private void loop() {
        double startTime = glfwGetTime();
        double fpsTimer = NULL;

        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();

            double currentTime = glfwGetTime();
            double elapsedTime = currentTime - startTime;
            fpsTimer += elapsedTime;
            startTime = currentTime;

            // Resize
            if (resized) {
                glViewport(0, 0, width, height);
                getActiveScreen().resize(width, height);
                resized = false;
                System.out.println("Resized to " + width + "x" + height);
            }

            // Fps calculating
            if (fpsTimer > 1) {
                int fps = (int) (1 / elapsedTime);
                fpsTimer = NULL;
                glfwSetWindowTitle(window, TITLE + " | fps: " + fps);
            }

            tick(elapsedTime);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            render();
            glfwSwapBuffers(window);
        }
    }

    private void initWindow() {
        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit())
            throw new RuntimeException("Could not initialize GLFW");

        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        if (MSAA) glfwWindowHint(GLFW_SAMPLES, SAMPLES);

        window = glfwCreateWindow(width, height, TITLE, NULL, NULL);
        if (window == NULL)
            throw new RuntimeException("Failed to create GLFW window");

        glfwMakeContextCurrent(window);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // Window size callback
        glfwSetWindowSizeCallback(window, (long window, int width, int height) -> {
            this.width = width;
            this.height = height;
            resized = true;
        });

        if (VSYNC)
            glfwSwapInterval(1);
        else
            glfwSwapInterval(0);

        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window, (vidMode.width() - width) / 2, (vidMode.height() - height) / 2);

        glfwShowWindow(window);
    }

    private void initGL() {
        GL.createCapabilities();

        glClearColor(243 / 255f, 245 / 255f, 163 / 255f, 1.0f);
        glEnable(GL_DEPTH_TEST);
    }

    private void cleanup() {
        getActiveScreen().cleanup();
        sounds.cleanup();
    }

    private Screen getActiveScreen() {
        switch (gameState) {
            case GAME:
                return gameScreen;
            case MENU:
                return menuScreen;
            case DEATH:
                return deathScreen;
            default:
                return null;
        }
    }

    private long getWindow() {
        return window;
    }

    public Camera getCamera() {
        return cam;
    }

    public void setActiveScreen(GameStates state) {
        if (gameState != state) {
            getActiveScreen().cleanup();
            gameState = state;
            getActiveScreen().show();
        }
    }

    private void run() {
        try {
            initWindow();
            initGL();
            initGame();
            loop();
        } finally {
            cleanup();
            glfwDestroyWindow(window);
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }
    }

    private void initGame() {
        menuScreen = new MenuScreen(this);
        gameScreen = new GameScreen(this);
        deathScreen = new DeathScreen(this);

        sounds = new Sounds();
        sounds.playMusic();

        input = new Input(getWindow());
    }

    public static void main(String[] args) {
        new Game().run();
    }
}