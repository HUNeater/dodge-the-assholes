#version 450 core

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;
out vec4 color;

struct Material {
	sampler2D diffuse;
	vec3 color;
	vec3 ambient;
	vec3 specular;
	float shininess;
};

struct Light {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 pos;
	
	float constant;
	float linear;
	float quadratic;
};

uniform Material mat;
uniform Light light;
uniform vec3 viewPos;
uniform bool useTexture;
uniform bool useFlatShading;

void main(void) {
    // alpha processing
    vec4 tex = texture(mat.diffuse, texCoord);
    if (useTexture) {
        if (tex.a < 0.5) {
             discard;
        }
    }

    if (!useFlatShading) {
    	vec3 norm = normalize(normal);
    	vec3 lightDir = normalize(light.pos - fragPos);
    	vec3 viewDir = normalize(viewPos - fragPos);
    	vec3 reflectDir = reflect(-lightDir, norm);

    	// ambient
    	vec3 ambient;
    	if(useTexture)
    	    ambient = light.ambient * vec3(tex);
    	else
    	    ambient = light.ambient * mat.ambient;

    	// diffuse
    	float diff = max(dot(norm, lightDir), 0.0);
    	vec3 diffuse;
    	if(useTexture)
           diffuse = light.diffuse * diff * vec3(tex);
        else
           diffuse = light.diffuse * (diff * mat.color);

    	// specular
    	float spec = pow(max(dot(viewDir, reflectDir), 0.0), mat.shininess);
    	vec3 specular = light.specular * (spec * mat.specular);

    	// attenuation
    	float dist = length(light.pos - fragPos);
    	float attenuation = 1.0f / (light.constant + light.linear * dist + light.quadratic * dist * dist);

    	ambient *= attenuation;
    	diffuse *= attenuation;
    	specular *= attenuation;

    	vec3 result = (ambient + diffuse + specular);
    	color = vec4(result, 1.0f);
    } else {
        if (useTexture) {
            color = texture(mat.diffuse, texCoord);
        } else {
            color = vec4(mat.color, 1.0f);
        }
    }
}