#version 450 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 norm;
layout (location = 2) in vec2 coord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

out vec3 normal;
out vec3 fragPos;
out vec2 texCoord;

void main(void) {
	gl_Position = projection * view * model * vec4(pos, 1.0f);
	
	fragPos = vec3(model * vec4(pos, 1.0f));
	normal = mat3(transpose(inverse(model))) * norm;
	texCoord = coord;
}